function randomPassword(length) {
    var symbols = "";
    var numbers = "";
    var lower = "";
    var upper = "";
    var chars = "";
    var pass = "";
    if (document.getElementById('symbols').checked){
    	symbols = "!@#$%^&*()_+";
    }
    if (document.getElementById('numbers').checked){
    	numbers = "1234567890";
    }
    if (document.getElementById('lower').checked){
    	lower = "abcdefghijklmnopqrstuvwxyz";
    }
    if (document.getElementById('upper').checked){
    	upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    }
    chars = lower + upper + numbers + symbols
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}

function generate() {
    myform.row_password.value = randomPassword(myform.length.value);
}