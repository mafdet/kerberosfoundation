# Kerberos Foundation

Here you will find the latest source for the site http://kerberosfoundation.com. Can find the css, js and html all in their own folders.
Utilizes Bootstrap 4 alpha, CSS3, HTML5, a bit of jquery and js

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Nothing I can think of

### Installing

If you want to host it locally.. should be able to browse via browser from root folder

## Built With

Bootstrap 4 alpha
CSS3
HTML5
jQuery
JavaScript

## Versioning

1.0

## Authors

* **k3rber0s**
